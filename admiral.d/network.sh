status=$( ip link | grep wlp2s0 | cut -d, -f3 )
	if [ $status = "UP" ]
	then
		signal=$( iw dev wlp2s0 station dump | grep "signal:" | cut -d' ' -f3 )
		if [ $signal -lt -50 ]
		then
			echo -n " "
		elif [ $signal -lt -30 ]
		then
			echo -n ""
		else
			echo -n ""
		fi
	fi
